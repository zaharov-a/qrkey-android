package xyz.a0x3a.qrkey;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import xyz.a0x3a.qrkey.Model.Project;
import xyz.a0x3a.qrkey.Model.Repository;

import static xyz.a0x3a.qrkey.PinActivity.ACTION_LOGIN;

public class ProjectListActivity extends AppCompatActivity {
    ArrayList<Project> projects;

    protected ListView projectList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        this.projectList = this.findViewById(R.id.projectList);


        // редактирование
        this.projectList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                long projectId = getProjectId(position);
                toEditProject(projectId);
                return true;
            }
        });

        // ввод пина
        this.projectList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long projectId = getProjectId(position);
                toPinInput(projectId);
            }
        });

        FloatingActionButton appendProjectBtn = this.findViewById(R.id.addProjectBtn);

        appendProjectBtn.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                toEditProject();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, this.getProjectList());
        this.projectList.setAdapter(adapter);
    }

    public void toPinInput(long projectId) {
        Intent intent = new Intent(this, PinActivity.class);
        intent.putExtra("projectId", projectId);
        intent.putExtra("action", ACTION_LOGIN);
        startActivity(intent);
    }

    public void toEditProject() {
        Intent intent = new Intent(this, ProjectActivity.class);
        startActivity(intent);
    }

    public void toEditProject(long projectId) {
        Intent intent = new Intent(this, ProjectActivity.class);
        intent.putExtra("projectId", projectId);
        startActivity(intent);
    }

    public String[] getProjectList() {
        ArrayList<String> list = new ArrayList<>();
        Repository repository = new Repository(this);

        this.projects = repository.getProjectList();

        for (Project project : this.projects) {
            list.add(project.getName());
        }

        String[] result = new String[list.size()];

        return list.toArray(result);
    }

    public Long getProjectId(int position) {
        Project project = this.projects.get(position);
        return project.getId();
    }
}
