package xyz.a0x3a.qrkey.Model;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpAsyncTask extends AsyncTask<String, Void, String[]> {
    private IHttpCompleted listener;

    public HttpAsyncTask(IHttpCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected String[] doInBackground(String... params) {
        String url = params[0];
        String json = params[1];

        String[] result = {"", ""};

        try {
            URL link = new URL(url);
            HttpURLConnection con = (HttpURLConnection) link.openConnection();

            con.setRequestMethod("POST");
            con.setDoOutput(true);

            OutputStream os = con.getOutputStream();
            os.write(json.getBytes());
            os.flush();
            os.close();

            result[0] = Integer.toString(con.getResponseCode());

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                result[1] = response.toString();
                // print result
//                        Log.d("post#response", response.toString());
//            } else {
//                        Log.d("post#response", result[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String[] result) {
        super.onPostExecute(result);
        this.listener.onTaskCompleted(result);
    }
}
