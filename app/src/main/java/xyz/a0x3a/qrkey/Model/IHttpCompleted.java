package xyz.a0x3a.qrkey.Model;

public interface IHttpCompleted {
    void onTaskCompleted(String[] result);
}
