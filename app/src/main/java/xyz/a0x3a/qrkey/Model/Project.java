package xyz.a0x3a.qrkey.Model;

import java.util.Date;

public class Project {

    private static final int NONCE_TYPE_COUNTER = 1;

    private static final int NONCE_TYPE_TIME = 2;

    private Long id = 0L;
    private String name;
    private String login = "";
    private Integer nonce = 0;
    private Integer nonceType = NONCE_TYPE_COUNTER;
    private String secret = "";

    public Project(String name) {
        this.name = name;
    }

    public Project(long id, String name, String login, int nonce, int nonceType, String secret) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.nonce = nonce;
        this.nonceType = nonceType;
        this.secret = secret;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getNonce() {
        if (this.nonceType == NONCE_TYPE_TIME) {
            Date date = new Date();
            return Math.round(date.getTime() / 1000);
        }
        return nonce;
    }

    public void setNonce(Integer nonce) {
        this.nonce = nonce;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Integer getNonceType() {
        return nonceType;
    }

    public void setNonceType(Integer nonceType) {
        this.nonceType = nonceType;
    }

    public Project incNonce() {
        this.nonce++;
        return this;
    }
}
