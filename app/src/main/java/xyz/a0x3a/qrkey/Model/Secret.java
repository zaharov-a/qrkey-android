package xyz.a0x3a.qrkey.Model;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.security.SecureRandom;

public class Secret {
    private static final int MIN = 1000;
    private static final int MAX = 10000;

    private String pin;
    private String deviceId;
    private String halfSecret;
    private String secret;

    public String getPin() {
        return pin;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getHalfSecret() {
        return halfSecret;
    }

    public String getSecret() {
        return secret;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void generate() {
        SecureRandom srand = new SecureRandom();
        int diff = MAX - MIN;
        Integer randNumber = MIN + srand.nextInt(diff + 1);

        this.halfSecret = this.hash(randNumber.toString());
        this.secret = this.hash(this.pin + this.deviceId + this.halfSecret);
    }

    private String hash(String str) {
        // какой-то пиздец DigestUtils.sha1Hex(""); не работает
        return new String(Hex.encodeHex(DigestUtils.sha1(str)));
    }
}
