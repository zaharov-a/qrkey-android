package xyz.a0x3a.qrkey.Model;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

public class Otp {

    private String nonce;
    private String pin;
    private String secret;
    private String deviceId;

    public Otp(String nonce, String pin, String secret, String deviceId) {
        this.nonce = nonce;
        this.pin = pin;
        this.secret = secret;
        this.deviceId = deviceId;
    }


    public String generate() {
        return this.getOtp(this.nonce + this.hash(this.pin + this.deviceId + this.secret));
    }

    private String getOtp(String str) {
        return this.hash(str).substring(0, 10);
    }

    private String hash(String str) {
        // какой-то пиздец DigestUtils.sha1Hex(""); не работает
        return new String(Hex.encodeHex(DigestUtils.sha1(str)));
    }
}
