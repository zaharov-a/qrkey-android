package xyz.a0x3a.qrkey.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;

public class Repository extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "qr-auth";

    private static final int DATABASE_VERSION = 1;

    public Repository(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE projects (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT NOT NULL, " +
                "login TEXT NOT NULL, " +
                "secret TEXT NOT NULL, " +
                "nonce INTEGER NOT NULL, " +
                "nonce_type INTEGER NOT NULL DEFAULT 1" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL("DROP TABLE projects");
//        this.onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<Project> getProjectList() {
        ArrayList<Project> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor result = db.rawQuery("SELECT * FROM projects ORDER BY name", null);

        if (result != null) {
            if (result.moveToFirst()) {
                do {
                    Project project = this.createFromCursor(result);
                    list.add(project);
                } while (result.moveToNext());
            }
            result.close();
        }

        db.close();

        return list;
    }

    public void addProject(Project project) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = this.getProjectValues(project);

        long id = db.insert("projects", null, values);

        project.setId(id);

        db.close();
    }

    public void updateProject(Project project) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = this.getProjectValues(project);

        String[] bind = {project.getId().toString()};

        db.update("projects", values, "id=?", bind);
        db.close();
    }

    public void deleteProject(Project project) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] bind = {project.getId().toString()};

        db.delete("projects", "id=?", bind);
        db.close();
    }

    public Project find(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] bind = {id.toString()};
        Cursor result = db.rawQuery("SELECT * FROM projects WHERE id=?", bind);

        if (result == null || !result.moveToFirst()) {
            return null;
        }

        Project project = this.createFromCursor(result);

        result.close();
        db.close();

        return project;
    }

    @NonNull
    private Project createFromCursor(Cursor result) {
        return new Project(
                result.getInt(result.getColumnIndex("id")),
                result.getString(result.getColumnIndex("name")),
                result.getString(result.getColumnIndex("login")),
                result.getInt(result.getColumnIndex("nonce")),
                result.getInt(result.getColumnIndex("nonce_type")),
                result.getString(result.getColumnIndex("secret"))
        );
    }

    private ContentValues getProjectValues(Project project) {
        ContentValues values = new ContentValues();

        values.put("name", project.getName());
        values.put("secret", project.getSecret());
        values.put("nonce", project.getNonce());
        values.put("nonce_type", project.getNonceType());
        values.put("login", project.getLogin());

        return values;
    }

}
