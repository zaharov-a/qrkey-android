package xyz.a0x3a.qrkey;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import xyz.a0x3a.qrkey.Model.HttpAsyncTask;
import xyz.a0x3a.qrkey.Model.IHttpCompleted;
import xyz.a0x3a.qrkey.Model.Otp;
import xyz.a0x3a.qrkey.Model.Project;
import xyz.a0x3a.qrkey.Model.Repository;
import xyz.a0x3a.qrkey.Model.Secret;

public class PinActivity extends AppCompatActivity implements IHttpCompleted {

    private PinLockListener mPinLockListener = new PinLockListener() {
//        String TAG = "";

        @Override
        public void onComplete(String pin) {
            scanQr(pin);
        }

        @Override
        public void onEmpty() {
//            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
//            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };

    public static final int ACTION_LOGIN = 1;
    public static final int ACTION_BIND = 2;

    private Repository repository = new Repository(this);

    //qr code scanner object
    private IntentIntegrator qrScan;
    private Long projectId;
    private Project project;
    private int action;
    private String pin;

    @Override
    protected void onCreate(Bundle savedInstanceState) throws NullPointerException {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        this.projectId = this.getIntent().getExtras().getLong("projectId");
        this.action = this.getIntent().getExtras().getInt("action");

        PinLockView mPinLockView = findViewById(R.id.pin_lock_view);
        mPinLockView.setPinLockListener(mPinLockListener);

        this.qrScan = new IntentIntegrator(this);

        this.qrScan.setOrientationLocked(false);

        this.qrScan.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) throws NullPointerException {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            this.pin = (String) this.qrScan.getMoreExtras().get("pin");
            this.projectId = (Long) this.qrScan.getMoreExtras().get("projectId");
            this.project = this.getProject(this.projectId);
            this.action = (Integer) this.qrScan.getMoreExtras().get("action");

            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                String url = result.getContents();
                if (this.action == ACTION_BIND) {
                    this.bindAction(url);
                } else {
                    this.loginAction(url);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void scanQr(String pin) {
        qrScan.addExtra("pin", pin);
        qrScan.addExtra("projectId", this.projectId);
        qrScan.addExtra("action", this.action);
        qrScan.initiateScan();
    }

    public String generateOtp() throws SecurityException {
        String deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("deviceId", deviceId);
        Log.d("OTP PARAMS", project.getNonce().toString() + "/" + this.pin + "/" + project.getSecret() + "/" + deviceId);
        Otp generator = new Otp(project.getNonce().toString(), this.pin, project.getSecret(), deviceId);

        this.repository.updateProject(project.incNonce());

        return generator.generate();
    }


    public void loginAction(String url) {
        String otp = this.generateOtp();
        try {
            JSONObject json = new JSONObject();
            json.put("otp", otp);
            json.put("login", this.project.getLogin());
            Log.d("sendOtp#json", json.toString());
            this.post(url, json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void bindAction(String url) {
        String deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Secret secret = new Secret();

        secret.setDeviceId(deviceId);
        secret.setPin(this.pin);

        secret.generate();
        project.setSecret(secret.getHalfSecret());

        try {
            JSONObject json = new JSONObject();
            json.put("secret", secret.getSecret());
            Log.d("sendOtp#json", json.toString());
            this.post(url, json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void post(String url, String json) {
        new HttpAsyncTask(this).execute(url, json);
    }

    public void postSuccess(String response) {
        if (this.action == ACTION_BIND) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                project.setLogin(jsonObject.getString("login"));
                project.setNonceType(jsonObject.getInt("nonce_type"));
                Log.d("=============", project.getLogin());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.repository.updateProject(project);
        Intent intent = new Intent(this, ProjectListActivity.class);
        startActivity(intent);
    }

    public void postError() {
        Toast.makeText(this, "Сервер не отвечает", Toast.LENGTH_LONG).show();
    }

    public Project getProject(long id) {
        return this.repository.find(id);
    }

    @Override
    public void onTaskCompleted(String[] result) {
        if (result[0].equals("200")) {
            postSuccess(result[1]);
        } else {
            postError();
        }
    }
}
