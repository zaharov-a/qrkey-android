package xyz.a0x3a.qrkey;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import xyz.a0x3a.qrkey.Model.Project;
import xyz.a0x3a.qrkey.Model.Repository;

import static xyz.a0x3a.qrkey.PinActivity.ACTION_BIND;

public class ProjectActivity extends AppCompatActivity {

    protected Repository repository = new Repository(this);
    protected Project project;
    protected EditText editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        long projectId = this.getIntent().getLongExtra("projectId", 0);

        this.project = this.getProject(projectId);
        this.editor = this.findViewById(R.id.editText);

        this.initData();

        this.bind();
    }

    protected Project getProject(long id) {
        if (id != 0) {
            return this.repository.find(id);
        }

        return new Project("Новый проект");
    }

    protected void initData() {
        this.editor.setText(this.project.getName());
    }

    protected void bind() {
        Button bindBtn = this.findViewById(R.id.bindDeviceBtn);
        Button removeBtn = this.findViewById(R.id.removeProjectBtn);

        bindBtn.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                toPinInput();
            }
        });
    }

    protected void toPinInput() {
        Intent intent = new Intent(this, PinActivity.class);

        this.saveProject();

        intent.putExtra("projectId", this.project.getId());
        intent.putExtra("action", ACTION_BIND);

        startActivity(intent);
    }

    protected void saveProject() {
        this.project.setNonce(0);
        this.project.setName(this.editor.getText().toString());
        if (this.project.getId() == 0) {
            this.repository.addProject(this.project);
        } else {
            this.repository.updateProject(this.project);
        }
    }
}
